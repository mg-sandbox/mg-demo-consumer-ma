sap.ui.define([
		"sap/ui/core/mvc/Controller"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller) {
		"use strict";

		return Controller.extend("mg.demo.prov.ma.fiorimodule.controller.App", {
			onInit: function () {

			}
		});
	});
