sap.ui.define([], function() {
    "use strict";

    // @ts-ignore
    jQuery.sap.declare("mg.demo.prov.ma");

    sap.ui.getCore().initLibrary({

        name: "mg.demo.prov.ma",
        version: "1.0.0",
        dependencies: ["sap.ui.core"],
        types: [],
        interfaces: [],
        controls: [
           "mg.demo.prov.ma.control.helloworld" 
        ],
        elements: [],
        noLibraryCSS: true
    });
    
	console.log("mg.demo.prov.ma library loaded...");

    // @ts-ignore
    return mg.demo.prov.ma;

}, false);