// @ts-nocheck

// https://stackoverflow.com/questions/56232557/sapui5-odata-v4-read

sap.ui.define([
    "sap/ui/core/Control",
    "sap/m/Text"
], function (Control, Text) {
    "use strict";
    return Text.extend("mg.demo.prov.ma.fiorimodule.control.hellobook", {
        metadata: {
            properties: {
                "applicationPath" : "string"
            },
            aggregations: {},
        },
        init: function(){
            this.setText("Hello books: One moment please");

            // Timeout is not a recommended solution, only for test purpose
            setTimeout(function(){
                const path = this.getApplicationPath() || sap.ui.require.toUrl("mg/demo/prov/ma/fiorimodule");
                const url =  path + "/srv-api/catalog/Books";
                console.log("Hellobook path: " + path);
                console.log("Hellobook url: " + url);
                $.get({
                    url: url,
                    success: function(oData) {
                        this.setText("Hello books: " + (oData.value.map(function(oValue){
                            return oValue.title;
                        }).join(", ") || "no books found..." ));
                    }.bind(this),
                    error: function(oError) {
                        console.error(oError);
                    }
                });
            }.bind(this),1000);
        },
        renderer : {},
    });
});
