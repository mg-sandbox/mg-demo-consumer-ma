sap.ui.define([
    "sap/ui/core/Control"

], function (Control) {
    "use strict";
    return Control.extend("mg.demo.prov.ma.fiorimodule.control.helloworld", {
        metadata: {
            properties: {},
            aggregations: {},
        },
        renderer: function (oRm, oControl) {
            oRm.write("<span>Hello World from Provider Managed Application</span>");
        }
    });
});
