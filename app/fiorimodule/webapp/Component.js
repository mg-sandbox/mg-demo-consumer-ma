/* eslint-disable no-undef */
// @ts-nocheck

/** We have to copy the library fiori application for local testing inside the consumer application
 *  This application will not be build and deployed on cloud foundry
 *  This code makes sure the library will be loaded from local or from HTML5 Application Runtime
 */
const studio = location.host.indexOf("studio") > -1;
const url = studio ? "../../mg-demo-provider-ma/fiorimodule" : "/mgdemoprovma.mgdemoprovmafiorimodule/";
    
/**
 * Load library of the fiori application, return a restricted object with only the defined controls...
 * Reference can be used in view > xmlns:control="mg.demo.prov.ma.control" and <control:helloworld/>
 * or controller > sap.ui.define(["mg/demo/prov/ma/control/helloworld"], function(control) { ... };
 * or controller > sap.ui.require(["mg/demo/prov/ma/control/helloworld"], function(control) { ... };
 */
//sap.ui.getCore().loadLibrary("mg.demo.prov.ma", url);
//console.log("Provider library loaded");

/**
 * Load config refers to the rootmap of the fiori application and doesn't provide an object or restrication compare to library.js
 * Reference can be used in view > xmlns:control="mgdemoprovma.control" and <control:helloworld/>
 * or controller > sap.ui.define(["mgdemoprovma/control/helloworld"], function(control) { ... };
 * or controller > sap.ui.require(["mgtestprovma/control/helloworld"], function(control) { ... };
 */
sap.ui.loader.config({ paths: { "mgdemoprovma": url }});
console.log("Provider configuration loaded");


sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"mg/demo/cons/ma/fiorimodule/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("mg.demo.cons.ma.fiorimodule.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
            this.setModel(models.createDeviceModel(), "device");
            
            // Create local jsonmodel for application path
            // Yes is better to create the JSON model from the manifest (keep it simple)
            var localModel = new sap.ui.model.json.JSONModel();
            localModel.setProperty("/applicationPath", sap.ui.require.toUrl("mg/demo/cons/ma/fiorimodule"));
            this.setModel(localModel, "local");
		}
	});
});
