sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "mgdemoprovma/control/helloworld",
	],

	function (Controller, hw2) {
		"use strict";

		return Controller.extend("mg.demo.cons.ma.fiorimodule.controller.App", {
			onInit: function () {
                
            },
            handleButton: function(){

                console.log("Loaded from define 'mgdemoprovma/control/helloworld' and hw2");
                console.log(new hw2());
                
                sap.ui.require(["mgdemoprovma/control/helloworld"], function(control) {
                    console.log("Loaded from require 'mgdemoprovma/control/helloworld'");
                    console.log(new control());
                });
                
                sap.ui.require(["mgdemoprovma/util/global"], function(global) {
                    console.log("Loaded from require 'mgdemoprovma/util/global'");
                    console.log("Global util is not available using the library reference");
                    global.showHelloWorld();
                });
            }
		});
	});
